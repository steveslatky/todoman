# TODOman
Manage your TODO as task in code.

# Put the right badges
[![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]

Stay inside code and track tasks and ideas all inside code.
Writing short fimialr syntax to get descripte tasks.

## Installation

``` Code
snippits 
```

## Usage example

A few motivating and useful examples of how your product can be used. Spice this
up with code blocks and potentially more screenshots.

_For more examples and usage, please refer to the [Wiki][wiki]._

## Development setup

Describe how to install all development dependencies and how to run an automated
test-suite of some kind. Potentially do this for multiple platforms.

```sh
make install
npm test
```

## Meta

Stephen Slatky – [@SteveSlatky](https://twitter.com/steveslatky) –
i@steveslatky.me

Distributed under the MIT license. See ``LICENSE`` for more information.

## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]:
https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]:
https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/yourname/yourproject/wiki

