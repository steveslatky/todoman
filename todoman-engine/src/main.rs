use std::fs::File;
use std::io::{Write, BufReader, BufRead, Error};
use std::{env, fs};

fn main() {
    println!("Hello, world!");
    read_file(&String::from("./test/hunter"));
}

fn read_file(path: &String) -> Result<(), Error> {

    let mut output = File::create(path)?;
    write!(output, "Rust\n💖\nFun")?;

    let input = File::open(path)?;
    let buffered = BufReader::new(input);

    for line in buffered.lines() {
	println!("{}", line?);
    }

    Ok(())
}

fn get_files_in_dir(dir: &String) -> Result<(), Error> {
    let current_dir = dir;

    for entry in fs::read_dir(current_dir)? {
        let entry = entry?;
        let path = entry.path();

        // TODO remove this stuff. Should I just read from here?
        if last_modified < 24 * 3600 && metadata.is_file() {
            println!(
                "Last modified: {:?} seconds, is read only: {:?}, size: {:?} bytes, filename: {:?}",
                last_modified,
                metadata.permissions().readonly(),
                metadata.len(),
                path.file_name().ok_or("No filename")?
            );
        }
    }

    Ok(())
}
